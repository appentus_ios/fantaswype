//
//  custom_shape_view.swift
//  FantaSwype
//
//  Created by Rajat Pathak on 13/08/19.
//  Copyright © 2019 appentus technologies pvt. ltd. All rights reserved.
//

import Foundation

import UIKit
@IBDesignable
class custom_shape_view: UIView {
    
    @IBInspectable var color : UIColor? = UIColor.gray {
        didSet {
            //            self.layer.backgroundColor = self.color?.cgColor
        }
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = UIColor.clear
        
    }
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        //get the size of the view
//        let size = self.bounds.size
//        //get 4 points for the shape layer
//        let p1 = self.bounds.origin
//        let p2 = CGPoint(x: p1.x, y: size.height)
//        let p3 = CGPoint(x: p2.x, y: size.height)
//        let p4 = CGPoint(x: p1.x + size.width, y: p1.y)
//
//        //create the path
//        let path = UIBezierPath()
//        path.move(to: p1)
//        path.addLine(to: p2)
//        path.addLine(to: p3)
//        path.addLine(to: p4)
//        path.close()
//        (color ?? UIColor.gray).set()
//        path.fill()
        let path = UIBezierPath()
        path.move(to: CGPoint(x: self.frame.width/2, y: 0.0))
        path.addLine(to: CGPoint(x: 0.0, y: self.frame.size.height))
        path.addLine(to: CGPoint(x: self.frame.size.width, y: self.frame.size.height))
        path.close()
        (color ?? UIColor.gray).set()
        path.fill()
    }
    
}
