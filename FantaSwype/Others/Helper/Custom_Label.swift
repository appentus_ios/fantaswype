//
//  CustomLabel.swift
//  FantaSwype
//
//  Created by appentus technologies pvt. ltd. on 8/9/19.
//  Copyright © 2019 appentus technologies pvt. ltd. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable class CustomLabel: UILabel {
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    @IBInspectable var border_Width: CGFloat = 4 {
        didSet {
            layer.borderWidth = border_Width
        }
    }
    
    @IBInspectable var border_Color: UIColor = UIColor.red {
        didSet {
            layer.borderColor = border_Color.cgColor
        }
    }
    
}
