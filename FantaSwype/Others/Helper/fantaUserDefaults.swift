//
//  fantaUserDefaults.swift
//  FantaSwype
//
//  Created by Rajat Pathak on 11/08/19.
//  Copyright © 2019 appentus technologies pvt. ltd. All rights reserved.
//

import Foundation

@objc class fantaUserDefaults: NSObject
{
  static let sharedInstance = fantaUserDefaults()
  
  private override init() {
    super.init()
    self.retriveUserFromUserDefaults()
  }
  
  
 
  var _user_image = String()
  
  var user_image: String {
    get {
      return _user_image
    }
    
    set {
      
      if _user_image != newValue {
        _user_image = newValue
        UserDefaults.standard.set(_user_image, forKey: "user_image")
        UserDefaults.standard.synchronize()
      }
    }
  }
  
 
  var _user_pass = String()
  
  var user_pass: String {
    get {
      return _user_pass
    }
    
    set {
      
      if _user_pass != newValue {
        _user_pass = newValue
        UserDefaults.standard.set(_user_pass, forKey: "user_pass")
        UserDefaults.standard.synchronize()
      }
    }
  }
  
  var _user_email = String()
  var user_email: String {
    get {
      return _user_email
    }
    
    set {
      
      if _user_email != newValue {
        _user_email = newValue
        UserDefaults.standard.set(_user_email, forKey: "user_email")
        UserDefaults.standard.synchronize()
      }
    }
  }
  
  var _user_name = String()
  var user_name: String {
    get {
      return _user_name
    }
    
    set {
      
      if _user_name != newValue {
        _user_name = newValue
        UserDefaults.standard.set(_user_name, forKey: "user_name")
        UserDefaults.standard.synchronize()
      }
    }
  }
  
  var _first_name = String()
  var first_name: String {
    get {
      return _first_name
    }
    
    set {
      
      if _first_name != newValue {
        _first_name = newValue
        UserDefaults.standard.set(_first_name, forKey: "first_name")
        UserDefaults.standard.synchronize()
      }
    }
  }
  
  var _last_name = String()
  var last_name: String {
    get {
      return _last_name
    }
    
    set {
      
      if _last_name != newValue {
        _last_name = newValue
        UserDefaults.standard.set(_last_name, forKey: "last_name")
        UserDefaults.standard.synchronize()
      }
    }
  }
  
  var _userId = String()
  
  var userId: String {
    get {
      return _userId
    }
    
    set {
      
      if _userId != newValue {
        _userId = newValue
        UserDefaults.standard.set(_userId, forKey: "userId")
        UserDefaults.standard.synchronize()
      }
    }
  }
  
  

  
  var _gender = String()
  
  var gender: String {
    get {
      return _gender
    }
    
    set {
      
      if _gender != newValue {
        _gender = newValue
        UserDefaults.standard.set(_gender, forKey: "gender")
        UserDefaults.standard.synchronize()
      }
    }
  }
  
  var _DOB = String()
  
  var DOB: String {
    get {
      return _DOB
    }
    
    set {
      
      if _DOB != newValue {
        _DOB = newValue
        UserDefaults.standard.set(_DOB, forKey: "DOB")
        UserDefaults.standard.synchronize()
      }
    }
  }
  
  
 
  
  func retriveUserFromUserDefaults() {
    let standardUserDefaults = UserDefaults.standard
    
    _last_name = standardUserDefaults.string(forKey: "last_name") != nil ? standardUserDefaults.string(forKey: "last_name")! : ""
    _first_name = standardUserDefaults.string(forKey: "first_name") != nil ? standardUserDefaults.string(forKey: "first_name")! : ""
    _userId = standardUserDefaults.string(forKey: "userId") != nil ? standardUserDefaults.string(forKey: "userId")! : ""
    _user_email = standardUserDefaults.string(forKey: "user_email") != nil ? standardUserDefaults.string(forKey: "user_email")! : ""
    _user_name = standardUserDefaults.string(forKey: "user_name") != nil ? standardUserDefaults.string(forKey: "user_name")! : ""
    _user_image = standardUserDefaults.string(forKey: "user_image") != nil ? standardUserDefaults.string(forKey: "user_image")! : ""
    _gender = standardUserDefaults.string(forKey: "gender") != nil ? standardUserDefaults.string(forKey: "gender")! : ""
    _DOB = standardUserDefaults.string(forKey: "DOB") != nil ? standardUserDefaults.string(forKey: "DOB")! : ""
    _user_pass = standardUserDefaults.string(forKey: "user_pass") != nil ? standardUserDefaults.string(forKey: "user_pass")! : ""
  
    
    standardUserDefaults.synchronize()
    
  }
  
  func remove_from_userdefaultds() {
 
    fantaUserDefaults.sharedInstance.last_name = ""
    fantaUserDefaults.sharedInstance.first_name = ""
    fantaUserDefaults.sharedInstance.userId = ""
    fantaUserDefaults.sharedInstance.user_email = ""
    fantaUserDefaults.sharedInstance.user_name = ""
    fantaUserDefaults.sharedInstance.user_image = ""
    fantaUserDefaults.sharedInstance.gender = ""
    fantaUserDefaults.sharedInstance.DOB = ""
    fantaUserDefaults.sharedInstance.user_pass = ""
    retriveUserFromUserDefaults()
  }
  
}
