//
//  Custom_TextField.swift
//  FantaSwype
//
//  Created by appentus technologies pvt. ltd. on 8/9/19.
//  Copyright © 2019 appentus technologies pvt. ltd. All rights reserved.
//

import Foundation

import UIKit

@IBDesignable
class Custom_TextField: UITextField {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    @IBInspectable var border_Width: CGFloat = 4 {
        didSet {
            layer.borderWidth = border_Width
        }
    }
    
    @IBInspectable var border_Color: UIColor = UIColor.red {
        didSet {
            layer.borderColor = border_Color.cgColor
        }
    }
    
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
    
    
    var padding = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    
    @IBInspectable var left: CGFloat = 0 {
        didSet {
            adjustPadding()
        }
    }
    
    @IBInspectable var right: CGFloat = 0 {
        didSet {
            adjustPadding()
        }
    }
    
    @IBInspectable var top: CGFloat = 0 {
        didSet {
            adjustPadding()
        }
    }
    
    @IBInspectable var bottom: CGFloat = 0 {
        didSet {
            adjustPadding()
        }
    }
    
    func adjustPadding() {
        padding = UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
        
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets(top: top, left: left, bottom: bottom, right: right))
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets(top: top, left: left, bottom: bottom, right: right))
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets(top: top, left: left, bottom: bottom, right: right))
    }
}


