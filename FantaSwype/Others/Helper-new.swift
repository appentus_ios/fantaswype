//  Helper.swift
//  FantaSwype
//  Created by appentus on 11/19/19.
//  Copyright © 2019 appentus technologies pvt. ltd. All rights reserved.


import Foundation
import UIKit


extension UIViewController {
    func push_To_VC(_ storyboard:String,_ vc_ID:String) {
        let storyboard = UIStoryboard(name:storyboard, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier:vc_ID)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func back_To_VC(_ sender:UIButton)  {
        self.navigationController?.popViewController(animated: true)
    }
    
}


func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt64 = 0
    Scanner(string: cString).scanHexInt64(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}



func func_attributed_String(_ normalText:String,boldText:String,fontSize:CGFloat) -> NSAttributedString {
    let attributedString = NSMutableAttributedString(string:normalText)
    let attrs = [NSAttributedString.Key.font : UIFont (name: "SegoeUI-Bold", size:fontSize)]
    let boldString = NSMutableAttributedString(string: boldText, attributes:attrs as [NSAttributedString.Key : Any])
    
    attributedString.append(boldString)
    
    return attributedString
}


extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
