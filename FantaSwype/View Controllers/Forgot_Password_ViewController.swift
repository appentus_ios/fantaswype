//  Forgot_Password_ViewController.swift
//  FantaSwype
//  Created by appentus on 11/20/19.
//  Copyright © 2019 appentus. All rights reserved.


import UIKit


class Forgot_Password_ViewController: UIViewController{
//    MARK:- Outlets
    @IBOutlet weak var txt_email:UITextField!
    
    @IBOutlet weak var img_email:UIImageView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //    MARK:- action methods
    @IBAction func btn_forgot_pwd(_ sender:UIButton) {
       push_To_VC("FantyaSwype", "Create_New_Password_ViewController")
    }
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

//    MARK:- textfield delegate methods
extension Forgot_Password_ViewController:UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txt_email {
            img_email.image = #imageLiteral(resourceName: "@-black.png")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txt_email {
            if txt_email.text!.isEmpty {
                img_email.image = #imageLiteral(resourceName: "@-light.png")
            } else {
                img_email.image = #imageLiteral(resourceName: "@-black.png")
            }
        }
    }
    
}
