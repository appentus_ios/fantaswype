//  Login_ViewController.swift
//  FantaSwype
//  Created by appentus on 11/20/19.
//  Copyright © 2019 appentus. All rights reserved.


import UIKit


class Login_ViewController: UIViewController {
    //    MARK:- Outlets
    @IBOutlet weak var txt_email:UITextField!
    @IBOutlet weak var txt_password:UITextField!
    
    @IBOutlet weak var img_email:UIImageView!
    @IBOutlet weak var img_password:UIImageView!
    
    @IBOutlet weak var btn_sign_up:UIButton!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //    MARK:- action methods
    @IBAction func btn_login(_ sender:UIButton) {
        self.view.endEditing(true)
    }
    
    @IBAction func btn_sign_up(_ sender:UIButton) {
        back_To_VC(sender)
    }
    
    @IBAction func btn_password(_ sender:UIButton) {
         push_To_VC("FantyaSwype", "Forgot_Password_ViewController")
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//    MARK:- textfield delegate methods
extension Login_ViewController:UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txt_email {
            img_email.image = #imageLiteral(resourceName: "@-black.png")
        } else if textField == txt_password {
            img_password.image = #imageLiteral(resourceName: "lock-black.png")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txt_email {
            if txt_email.text!.isEmpty {
                img_email.image = #imageLiteral(resourceName: "@-light.png")
            } else {
                img_email.image = #imageLiteral(resourceName: "@-black.png")
            }
        } else if textField == txt_password {
            if txt_password.text!.isEmpty {
                img_password.image = #imageLiteral(resourceName: "lock-light.png")
            } else {
                img_password.image = #imageLiteral(resourceName: "lock-black.png")
            }
        }
    }
    
}
