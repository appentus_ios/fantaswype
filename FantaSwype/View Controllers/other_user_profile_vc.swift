//
//  other_user_profile_vc.swift
//  FantaSwype
//
//  Created by Ayush Pathak on 27/11/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class other_user_profile_vc: UIViewController {

    @IBOutlet weak var coll_view: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        coll_view.contentInsetAdjustmentBehavior = .never
        tabController.setBar(hidden: true, animated: true)
    }
    
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension other_user_profile_vc: UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "other_profile_cell", for: indexPath) as! other_profile_cell
        if indexPath.row == 6 || indexPath.row == 0 {
            cell.featured_view.isHidden = false
        }else{
           cell.featured_view.isHidden = true
        }
        cell.view_featured_text.roundCorners(corners: [.bottomRight,.topRight], radius: 15)
        
        if indexPath.row == 1{
            cell.img_one.isHidden = false
            cell.img_two.isHidden = false
            cell.img_three.isHidden = true
            cell.img_four.isHidden = true
        }else if indexPath.row == 5{
            cell.img_one.isHidden = false
            cell.img_two.isHidden = false
            cell.img_three.isHidden = false
            cell.img_four.isHidden = true
        }else{
            cell.img_one.isHidden = false
            cell.img_two.isHidden = false
            cell.img_three.isHidden = false
            cell.img_four.isHidden = false
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "profile_other_coll_view", for: indexPath) as! profile_other_coll_view
//            headerView.roundCorners(corners: [.bottomRight,.bottomLeft], radius: 40)
            
            return headerView
        default:
            assert(false, "Invalid element type")
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width / 2)-12, height: 120)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    }
    
}






