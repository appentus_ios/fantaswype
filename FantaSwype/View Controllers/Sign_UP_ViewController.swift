//
//  Sign_In_ViewController.swift
//  FantaSwype
//
//  Created by appentus on 11/19/19.
//  Copyright © 2019 appentus technologies pvt. ltd. All rights reserved.
//

import UIKit

class Sign_UP_ViewController: UIViewController {

    //    MARK:- Outlets
    @IBOutlet weak var view_check_height: UIView!
    @IBOutlet weak var height_view: NSLayoutConstraint!
    
    @IBOutlet weak var txt_user_name:UITextField!
    @IBOutlet weak var txt_email:UITextField!
    @IBOutlet weak var txt_password:UITextField!
    
    @IBOutlet weak var img_user_name:UIImageView!
    @IBOutlet weak var img_email:UIImageView!
    @IBOutlet weak var img_password:UIImageView!
    
    @IBOutlet weak var btn_sign_in:UIButton!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    //    MARK:- action methods
    @IBAction func btn_create_ac(_ sender:UIButton) {
        self.view.endEditing(true)
    }
    
    @IBAction func btn_sign_in(_ sender:UIButton) {
        push_To_VC("FantyaSwype", "Login_ViewController")
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//    MARK:- textfield delegate methods
extension Sign_UP_ViewController:UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txt_user_name {
            img_user_name.image = #imageLiteral(resourceName: "user-light.png")
        } else if textField == txt_email {
            img_email.image = #imageLiteral(resourceName: "@-black.png")
        } else if textField == txt_password {
            img_password.image = #imageLiteral(resourceName: "lock-black.png")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txt_user_name {
            if txt_user_name.text!.isEmpty {
                img_user_name.image = #imageLiteral(resourceName: "user-light.png")
            } else {
                img_user_name.image = #imageLiteral(resourceName: "user-black.png")
            }
        } else if textField == txt_email {
            if txt_email.text!.isEmpty {
                img_email.image = #imageLiteral(resourceName: "@-light.png")
            } else {
                img_email.image = #imageLiteral(resourceName: "@-black.png")
            }
        } else if textField == txt_password {
            if txt_password.text!.isEmpty {
                img_password.image = #imageLiteral(resourceName: "lock-light.png")
            } else {
                img_password.image = #imageLiteral(resourceName: "lock-black.png")
            }
        }
    }
    
}
