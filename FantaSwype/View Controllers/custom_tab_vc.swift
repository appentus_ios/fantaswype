//
//  custom_tab_vc.swift
//  FantaSwype
//
//  Created by Ayush Pathak on 23/11/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

var tabController:AZTabBarController!

class custom_tab_vc: UIViewController {

    var counter = 0
    var icons = [UIImage]()
    var sIcons = [UIImage]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        icons.append(UIImage(named: "Add 1x")!)
        icons.append(UIImage(named: "Home 1x")!)
        icons.append(UIImage(named: "Notification 1x")!)
        icons.append(UIImage(named: "Popular 1x")!)
        icons.append(UIImage(named: "Profile 1x")!)
        
        sIcons.append(UIImage(named:  "Add 1xs")!)
        sIcons.append(UIImage(named:  "Home 1xs")!)
        sIcons.append(UIImage(named:  "Notification 1xs")!)
        sIcons.append(UIImage(named:  "Popular 1xs")!)
        sIcons.append(UIImage(named:  "Profile 1xs")!)
        
        
        
         //init
        //tabController = .insert(into: self, withTabIconNames: icons)
        tabController = .insert(into: self, withTabIcons: icons, andSelectedIcons: sIcons)
        
        //set delegate
        tabController.delegate = self
        
        //set child controllers
        
        let colorController = self.storyboard?.instantiateViewController(withIdentifier: "Home_ViewController") as! Home_ViewController
        let nav1 = UINavigationController(rootViewController: colorController)
        nav1.navigationBar.isTranslucent = false
        
        let vc_two = UIViewController()
        let nav2 = UINavigationController(rootViewController: vc_two)
        nav2.navigationBar.isTranslucent = false
        
        let vc_three = UIViewController()
        let nav3 = UINavigationController(rootViewController: vc_three)
        nav3.navigationBar.isTranslucent = false
        
        let vc_four = UIViewController()
        let nav4 = UINavigationController(rootViewController: vc_four)
        nav4.navigationBar.isTranslucent = false
        
        let vc_five = UIViewController()
        let nav5 = UINavigationController(rootViewController: vc_five)
        nav5.navigationBar.isTranslucent = false
        
        tabController.setViewController(nav1, atIndex: 0)
        tabController.setViewController(nav2, atIndex: 1)
        tabController.setViewController(nav3, atIndex: 2)
        tabController.setViewController(nav4, atIndex: 3)
        tabController.setViewController(nav5, atIndex: 4)
        tabController.buttonsBackgroundColor = .clear
        tabController.tabBarHeight = 60
        tabController.setIndex(0, animated: true)
        tabController.animateTabChange = false
        tabController.onlyShowTextForSelectedButtons = false
        tabController.setTitle("", atIndex: 0)
        tabController.setTitle("", atIndex: 1)
        tabController.setTitle("", atIndex: 2)
        tabController.setTitle("", atIndex: 3)
        tabController.setTitle("", atIndex: 4)
        tabController.font = UIFont(name: "SegoeUI-Regular", size: 12)
        tabController.setButtonTintColor(color: hexStringToUIColor(hex: "#0059B3"), atIndex: 0)
        tabController.buttonsBackgroundColor = .clear
        tabController.selectionIndicatorColor = .clear
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.8) {
            tabController.buttons[0].setImage(UIImage(named: "Add 1xs") , for: UIControl.State.selected)
        }
        tabController.selectionIndicatorHeight = 5
        tabController.selectionIndicatorColor = hexStringToUIColor(hex: "#0059B3")
        
    }
    

}

extension custom_tab_vc:AZTabBarDelegate{
    func tabBar(_ tabBar: AZTabBarController, didSelectTabAtIndex index: Int) {
        for i in 0..<5{
            tabController.setButtonTintColor(color: UIColor.clear, atIndex: i)
            tabController.buttons[i].setImage(self.icons[i] , for: UIControl.State.normal)
        }
        tabController.setButtonTintColor(color: hexStringToUIColor(hex: "#0059B3"), atIndex: index)
        tabController.buttons[index].setImage(self.sIcons[index] , for: UIControl.State.selected)
        
    }
}
