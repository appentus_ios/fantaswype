//
//  Create_New_Password_ViewController.swift
//  FantaSwype
//
//  Created by appentus on 11/20/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class Create_New_Password_ViewController: UIViewController {
    //    MARK:- Outlets
    @IBOutlet weak var txt_password:UITextField!
    @IBOutlet weak var txt_confirm_password:UITextField!
    
    @IBOutlet weak var img_password:UIImageView!
    @IBOutlet weak var img_confirm_password:UIImageView!
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    //    MARK:- action methods
    @IBAction func btn_change_password(_ sender:UIButton) {
        self.view.endEditing(true)
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//    MARK:- textfield delegate methods
extension Create_New_Password_ViewController:UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txt_password {
            img_password.image = #imageLiteral(resourceName: "lock-black.png")
        } else if textField == txt_confirm_password {
            img_confirm_password.image = #imageLiteral(resourceName: "lock-black.png")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
         if textField == txt_password {
            if txt_password.text!.isEmpty {
                img_password.image = #imageLiteral(resourceName: "lock-light.png")
            } else {
                img_password.image = #imageLiteral(resourceName: "lock-black.png")
            }
         } else if textField == txt_confirm_password {
            if txt_confirm_password.text!.isEmpty {
                img_confirm_password.image = #imageLiteral(resourceName: "lock-light.png")
            } else {
                img_confirm_password.image = #imageLiteral(resourceName: "lock-black.png")
            }
        }
    }
    
}
