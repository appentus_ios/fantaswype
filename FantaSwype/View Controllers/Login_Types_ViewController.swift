//  Login_Types.swift
//  FantaSwype
//  Created by appentus on 11/19/19.
//  Copyright © 2019 appentus technologies pvt. ltd. All rights reserved.


import Foundation
import UIKit


class Login_Types_ViewController: UIViewController {
    
    //    MARK:- Outlets
    @IBOutlet weak var btn_sign_in:UIButton!
    @IBOutlet weak var scroll_view: UIScrollView!
    @IBOutlet weak var height_view: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let title_sign_in = func_attributed_String("Already have an account?", boldText:" Sign in", fontSize: 14)
        btn_sign_in.setAttributedTitle(title_sign_in, for: .normal)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.05) {
            self.set_view_height()
        }
    }
    
//    MARK:- Action
    @IBAction func btn_skip(_ sender:UIButton) {
        push_To_VC("FantyaSwype", "custom_tab_vc")
    }
    
    @IBAction func btn_sign_up_FB(_ sender:UIButton) {
        
    }
    
    @IBAction func btn_sign_up_Gmail(_ sender:UIButton) {
        
    }
    
    @IBAction func btn_sign_up_email(_ sender:UIButton) {
        push_To_VC("FantyaSwype", "Sign_UP_ViewController")
    }
    
    @IBAction func btn_sign_in(_ sender:UIButton) {
        push_To_VC("FantyaSwype", "Login_ViewController")
    }
    
    func set_view_height() {
        let device = UIDevice.modelName
        if device == "iPhone X" || device == "iPhone XS" || device == "iPhone XS Max" || device == "iPhone XR" || device == "iPhone 11" || device == "iPhone 11 Pro" || device == "iPhone 11 Pro Max" || device == "Simulator iPhone 11"{
            height_view.constant = scroll_view.frame.height
            self.scroll_view.isScrollEnabled = false
        }else{
            height_view.constant = 768.0
            self.scroll_view.isScrollEnabled = true
        }
    }
    
}

