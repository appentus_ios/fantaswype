//
//  profile_other_coll_view.swift
//  FantaSwype
//
//  Created by Ayush Pathak on 27/11/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class profile_other_coll_view: UICollectionReusableView {
        
    @IBOutlet weak var bottom_view: UIView!
    @IBOutlet weak var person_img: UIView!
    @IBOutlet weak var name_lbl: UILabel!
    @IBOutlet weak var post_lbl: UILabel!
    @IBOutlet weak var follower_lbl: UILabel!
    @IBOutlet weak var following_lbl: UILabel!
}
