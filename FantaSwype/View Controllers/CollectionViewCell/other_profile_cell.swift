//
//  other_profile_cell.swift
//  FantaSwype
//
//  Created by Ayush Pathak on 27/11/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class other_profile_cell: UICollectionViewCell {
    @IBOutlet weak var img_one: UIImageView!
    @IBOutlet weak var img_two: UIImageView!
    @IBOutlet weak var img_three: UIImageView!
    @IBOutlet weak var img_four: UIImageView!

    @IBOutlet weak var view_featured_text: UIView!
    @IBOutlet weak var featured_view: UIView!
}
