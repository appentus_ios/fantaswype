//
//  Splash_ViewController.swift
//  FantaSwype
//
//  Created by appentus on 11/19/19.
//  Copyright © 2019 appentus technologies pvt. ltd. All rights reserved.
//

import UIKit

class Splash_ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
            self.push_To_VC("FantyaSwype","WalkThrough_ViewController")
        }
    }
    
}



