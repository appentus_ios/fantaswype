//  Home_ViewController.swift
//  FantaSwype
//  Created by appentus on 11/20/19.
//  Copyright © 2019 appentus. All rights reserved.


import UIKit
import KUIPopOver

class Home_ViewController: UIViewController {
    //    MARK:- Outlets
    @IBOutlet weak var tbl_home:UITableView!
    //    MARK:- vars
    var index_scrolling = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        tabController.setBar(hidden: false, animated: true)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    //    MARK:- IBAction
    @IBAction func btn_more_option(_ sender:UIButton) {
        let popOverViewController = DefaultPopOverViewController()
        popOverViewController.preferredContentSize = CGSize(width: 200.0, height: 300.0)
        popOverViewController.popoverPresentationController?.sourceView = sender
        let customView = UINib(nibName: "More_Option", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! More_Option
        
        customView.frame = popOverViewController.view.frame //CGRect(origin: CGPoint(x: 0.0, y: 0.0), size: CGSize(width: 200.0, height: 300.0))
        customView.view_profile_btn.addTarget(self, action: #selector(view_profile_btn(_:)), for: .touchUpInside)
        customView.share_btn.addTarget(self, action: #selector(share_btn(_:)), for: .touchUpInside)
        customView.follow_btn.addTarget(self, action: #selector(follow_btn(_:)), for: .touchUpInside)
        customView.block_btn.addTarget(self, action: #selector(block_btn(_:)), for: .touchUpInside)
        customView.report_btn.addTarget(self, action: #selector(report_btn(_:)), for: .touchUpInside)
        
        customView.center = popOverViewController.view.center
        popOverViewController.view.addSubview(customView)
        popOverViewController.popoverPresentationController?.sourceRect = sender.bounds
        present(popOverViewController, animated: true, completion: nil)
        
        
    }
    
//    MARK: popup actions
        @objc func view_profile_btn(_ sender:UIButton){
            dismiss(animated: true) {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "other_user_profile_vc") as! other_user_profile_vc
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        @objc func share_btn(_ sender:UIButton){
            dismiss(animated: true, completion:  nil)
        }
        @objc func follow_btn(_ sender:UIButton){
            dismiss(animated: true, completion:  nil)
        }
        @objc func block_btn(_ sender:UIButton){
            dismiss(animated: true, completion:  nil)
        }
        @objc func report_btn(_ sender:UIButton){
            dismiss(animated: true, completion:  nil)
        }
            
}



//MARK:- tableview methods
extension Home_ViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row%2 == 0 {
            return tbl_home.frame.size.height
        } else if indexPath.row%3 == 1 {
            return 465
        } else {
            return 345
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row%2 == 0 {
            tableView.register(UINib(nibName: "Home_4_TableViewCell", bundle: nil), forCellReuseIdentifier: "cell-4")
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-4", for: indexPath) as! Home_4_TableViewCell
            
            cell.btn_more_option.addTarget(self, action: #selector(btn_more_option(_:)), for: .touchUpInside)
            
            return cell
        } else if indexPath.row%3 == 1 {
            tableView.register(UINib(nibName: "Home_3_TableViewCell", bundle: nil), forCellReuseIdentifier: "cell-3")
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-3", for: indexPath) as! Home_3_TableViewCell
            
            cell.btn_more_option.addTarget(self, action: #selector(btn_more_option(_:)), for: .touchUpInside)
            
            return cell
        } else {
            tableView.register(UINib(nibName: "Home_2_TableViewCell", bundle: nil), forCellReuseIdentifier: "cell-2")
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell-2", for: indexPath) as! Home_2_TableViewCell
            
            cell.btn_more_option.addTarget(self, action: #selector(btn_more_option(_:)), for: .touchUpInside)
            
            return cell
        }
        
//        tableView.register(UINib(nibName: "Home_4_TableViewCell", bundle: nil), forCellReuseIdentifier: "cell-4")
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cell-4", for: indexPath) as! Home_4_TableViewCell
//        tableView.register(UINib(nibName: "Home_3_TableViewCell", bundle: nil), forCellReuseIdentifier: "cell-3")
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cell-3", for: indexPath) as! Home_3_TableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
//
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        index_scrolling += 1
//        let indexPath = IndexPath (item:index_scrolling, section: 0)
//        tbl_home.scrollToRow(at: indexPath, at: .top, animated: true)
//    }
    
//    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        
//        // UITableView only moves in one direction, y axis
//        let currentOffset = scrollView.contentOffset.y
//        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
//        
//        // Change 10.0 to adjust the distance from bottom
//        if maximumOffset - currentOffset <= 10.0 {
//            self.loadMore()
//        }
//    }
    
}
