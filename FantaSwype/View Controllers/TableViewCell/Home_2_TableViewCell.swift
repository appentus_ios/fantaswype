//
//  Home_2_TableViewCell.swift
//  FantaSwype
//
//  Created by appentus on 11/22/19.
//  Copyright © 2019 appentus. All rights reserved.
//



import UIKit
import Cheers
import Pulsator


class Home_2_TableViewCell: UITableViewCell {
//    MARK:-
    @IBOutlet weak var btn_more_option:UIButton!
    
    @IBOutlet weak var view_1:UIView!
    @IBOutlet weak var view_2:UIView!

    @IBOutlet weak var left_width_1:NSLayoutConstraint!
    @IBOutlet weak var left_width_2:NSLayoutConstraint!

    @IBOutlet weak var right_width_1:NSLayoutConstraint!
    @IBOutlet weak var right_width_2:NSLayoutConstraint!
    
    @IBOutlet var arr_lbl_bench:[UILabel]!
    @IBOutlet var arr_lbl_start:[UILabel]!
    
    @IBOutlet var arr_view_pulsator_container:[UIView]!
    @IBOutlet var arr_view_pulsator:[UIView]!
    @IBOutlet var arr_lbl_percent:[UILabel]!


//    let pulsator = Pulsator()

    var pulseEffect:LFTPulseAnimation!

    var pulseArray = [CAShapeLayer]()

    override func awakeFromNib() {
        super.awakeFromNib()

        for i in 0..<arr_view_pulsator_container.count {
            let view_pulsator_container = arr_view_pulsator_container[i]
            view_pulsator_container.isHidden = true
        }

        for i in 0..<arr_view_pulsator.count {
            let view_pulsator = arr_view_pulsator[i]
            view_pulsator.isHidden = true

            let pulsator = Pulsator()
            view_pulsator.layer.addSublayer(pulsator)

            pulsator.numPulse = 5
            pulsator.radius = 55
            pulsator.animationDuration = 4

            if i == 0 {
              pulsator.backgroundColor = hexStringToUIColor(hex: "77DE08").cgColor
            } else {
              pulsator.backgroundColor = hexStringToUIColor(hex: "FD9104").cgColor
            }

            pulsator.start()
            pulsator.position = CGPoint(x:view_pulsator.frame.size.width / 2.0, y:view_pulsator.frame.size.width / 2.0)

            for lbl_percent in arr_lbl_percent {
                view_pulsator.bringSubviewToFront(lbl_percent)
                lbl_percent.layer.cornerRadius = lbl_percent.frame.width/2
                lbl_percent.clipsToBounds = true
            }
        }

        for view_voting in arr_view_pulsator {
            view_voting.layer.cornerRadius = view_voting.frame.width/2
            view_voting.clipsToBounds = true
        }

        for lbl_bench in arr_lbl_bench {
            lbl_bench.transform = CGAffineTransform(rotationAngle: -(CGFloat.pi / 2))
        }

        for lbl_start in arr_lbl_start {
            lbl_start.transform = CGAffineTransform(rotationAngle: -(CGFloat.pi / 2))
        }

        let panGesture_1 = UIPanGestureRecognizer(target: self, action: #selector(self.pan_View_1))
        let panGesture_2 = UIPanGestureRecognizer(target: self, action: #selector(self.pan_View_2))
        
        left_width_1.constant = 0.0
        right_width_1.constant = 0.0
        
        left_width_2.constant = 0.0
        right_width_2.constant = 0.0
        
        view_1.addGestureRecognizer(panGesture_1)
        view_2.addGestureRecognizer(panGesture_2)
        panGesture_1.delegate = self
        panGesture_2.delegate = self
    }

    

    func setupInitialValues(_ color:UIColor) {
//        pulsator.numPulse = 5
//        pulsator.radius = 55
//        pulsator.animationDuration = 4
//        pulsator.backgroundColor = color.cgColor
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    func createPulse(imgvAvatar:UIView) {
        for _ in 0...2 {
            let circularPath = UIBezierPath(arcCenter:.zero, radius:((imgvAvatar.superview?.frame.size.width )! )/2, startAngle: 0, endAngle: 2 * .pi , clockwise: true)
            let pulsatingLayer = CAShapeLayer()
            pulsatingLayer.path = circularPath.cgPath
            pulsatingLayer.lineWidth = 10
            pulsatingLayer.fillColor = hexStringToUIColor(hex: "77DE08").cgColor
            pulsatingLayer.lineCap = CAShapeLayerLineCap.round
            pulsatingLayer.position = CGPoint(x: imgvAvatar.frame.size.width / 2.0, y: imgvAvatar.frame.size.width / 2.0)
            imgvAvatar.layer.addSublayer(pulsatingLayer)
            pulseArray.append(pulsatingLayer)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.animatePulsatingLayerAt(index: 0)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                self.animatePulsatingLayerAt(index: 1)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    self.animatePulsatingLayerAt(index: 2)
                })
            })
        })
    }

    

    func animatePulsatingLayerAt(index:Int) {
        //Giving color to the layer
        pulseArray[index].strokeColor = hexStringToUIColor(hex: "77DE08").cgColor
        
        //Creating scale animation for the layer, from and to value should be in range of 0.0 to 1.0
        // 0.0 = minimum
        //1.0 = maximum
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.fromValue = 0.0
        scaleAnimation.toValue = 0.9
        
        //Creating opacity animation for the layer, from and to value should be in range of 0.0 to 1.0
        // 0.0 = minimum
        //1.0 = maximum
        let opacityAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
        opacityAnimation.fromValue = 0.9
        opacityAnimation.toValue = 0.0
        
        // Grouping both animations and giving animation duration, animation repat count
        let groupAnimation = CAAnimationGroup()
        groupAnimation.animations = [scaleAnimation, opacityAnimation]
        groupAnimation.duration = 4
        groupAnimation.repeatCount = 1000//.greatestFiniteMagnitude
        groupAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        //adding groupanimation to the layer
        pulseArray[index].add(groupAnimation, forKey: "groupanimation")
    }
    
}



extension Home_2_TableViewCell {
    @objc func pan_View_1(_ sender:UIPanGestureRecognizer) {
        let point = sender.location(in: self.view_1)
        let loc_from_center = sender.translation(in: self.view_1)
//        print(loc_from_center.x)
//        print(loc_from_center.y)
        let x = loc_from_center.x
        let y = loc_from_center.y

        if x > 0{
            left_width_1.constant = x
            right_width_2.constant = x

            right_width_1.constant = 0
            left_width_2.constant = 0
        } else {
            right_width_1.constant = -(x)
            left_width_2.constant = -(x)

            left_width_1.constant = 0
            right_width_2.constant = 0
        }

        //        bench_up_width.constant = 0.0
        //        start_down_widtg.constant = 0.0

        if sender.state == .ended {
            if x>0 {
                if x >= (self.view_1.frame.width / 2){
                    UIView.animate(withDuration: 0.3) {
                        self.left_width_1.constant = UIScreen.main.bounds.width
                        self.right_width_2.constant = UIScreen.main.bounds.width
                        self.layoutIfNeeded()
                    }
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.3) {
                        self.left_width_1.constant = 0.0
                        self.right_width_2.constant = 0.0
//                        self.view_up_after.isHidden = false
//                        self.view_bottom_after.isHidden = false
                        self.view_1.gestureRecognizers?.removeAll()
                        self.view_2.gestureRecognizers?.removeAll()
                        self.show_animation(view:self.view_2)
                    }
                } else if x <= (self.view_1.frame.width / 2){
                    UIView.animate(withDuration: 0.3) {
                        self.left_width_1.constant = 0.0
                        self.right_width_2.constant = 0.0
                        self.layoutIfNeeded()
                    }
                }
            } else {
                if abs(x) >= (self.view_1.frame.width / 2){
                    UIView.animate(withDuration: 0.3) {
                        self.left_width_2.constant = UIScreen.main.bounds.width
                        self.right_width_1.constant = UIScreen.main.bounds.width

                        self.layoutIfNeeded()
                    }
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.3) {
                        self.left_width_2.constant = 0.0
                        self.right_width_1.constant = 0.0
//                        self.view_up_after.isHidden = false
//                        self.view_bottom_after.isHidden = false
                        self.view_1.gestureRecognizers?.removeAll()
                        self.view_2.gestureRecognizers?.removeAll()
                        self.show_animation(view:self.view_1)
                    }
                } else if abs(x) <= (self.view_1.frame.width / 2) {
                    UIView.animate(withDuration: 0.3) {
                        self.left_width_2.constant = 0.0
                        self.right_width_1.constant = 0.0
                        self.layoutIfNeeded()
                    }
                }
            }
        }
    }

    

    @objc func pan_View_2(_ sender:UIPanGestureRecognizer) {
        let loc_from_center = sender.translation(in: self.view_1)

        let point = sender.translation(in: self.view_2)
        let x = point.x
        let y = point.y

        if x > 0 {
            right_width_1.constant = x
            left_width_2.constant = x
            left_width_1.constant = 0
            right_width_2.constant = 0
        } else {
            left_width_1.constant = -(x)
            right_width_2.constant = -(x)
            right_width_1.constant = 0
            left_width_2.constant = 0
        }

        //        width_start.constant = 0.0
        //        width_bench.constant = 0.0

        if sender.state == .ended {
            if x>0 {
                if x >= (self.view_1.frame.width / 2){
                    UIView.animate(withDuration: 0.3) {
                        self.left_width_2.constant = UIScreen.main.bounds.width
                        self.right_width_1.constant = UIScreen.main.bounds.width
                        self.layoutIfNeeded()
                    }

                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.3) {
                        self.left_width_2.constant = 0.0
                        self.right_width_1.constant = 0.0

//                        self.view_up_after.isHidden = false
//                        self.view_bottom_after.isHidden = false
                        self.view_1.gestureRecognizers?.removeAll()
                        self.view_2.gestureRecognizers?.removeAll()
//                        self.stack_view.spacing = 1.0
//                        self.show_animation(view: self.stack_view)
                    }
//                    id_sel = 1
//                    give_vote()
                }else if abs(x) <= (self.view_1.frame.width / 2){
                    UIView.animate(withDuration: 0.3) {
                        self.left_width_2.constant = 0.0
                        self.right_width_1.constant = 0.0
                        self.layoutIfNeeded()
                    }
                }
            }else{
                if abs(x) >= (self.view_1.frame.width / 2){
                    UIView.animate(withDuration: 0.3) {
                        self.left_width_1.constant = UIScreen.main.bounds.width
                        self.right_width_2.constant = UIScreen.main.bounds.width
                        self.layoutIfNeeded()
                    }
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.3) {
                        self.left_width_1.constant = 0.0
                        self.right_width_2.constant = 0.0

//                        self.view_up_after.isHidden = false
//                        self.view_bottom_after.isHidden = false
                        self.view_1.gestureRecognizers?.removeAll()
                        self.view_2.gestureRecognizers?.removeAll()
//                        self.stack_view.spacing = 1.0
//                        self.show_animation(view: self.stack_view)
                    }
//                    id_sel = 0
//                    give_vote()
                } else if x <= (self.view_1.frame.width / 2) {
                    UIView.animate(withDuration: 0.3) {
                        self.left_width_1.constant = 0.0
                        self.right_width_2.constant = 0.0
                        self.layoutIfNeeded()
                    }
                }
            }
        }
    }
}



extension Home_2_TableViewCell {
    func show_animation(view : UIView) {
        for i in 0..<arr_view_pulsator_container.count {
            let view_pulsator_container = arr_view_pulsator_container[i]
            view_pulsator_container.isHidden = false
        }
        
        var cheerView = CheerView()
        let frame = CGRect (x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        cheerView = CheerView.init(frame:frame)
        view.addSubview(cheerView)
        cheerView.config.particle = .confetti(allowedShapes: Particle.ConfettiShape.all)
        cheerView.start()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.5) {
            cheerView.stop()
            cheerView.removeFromSuperview()
        }
    }
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let panGestureRecognizer = gestureRecognizer as? UIPanGestureRecognizer {
            //let translation = panGestureRecognizer.translation(in: superview!)
            let translation = panGestureRecognizer.translation(in: superview)
            if abs(translation.x) > abs(translation.y) {
                return true
            }
            return false
        }
        return false
    }
    
}
