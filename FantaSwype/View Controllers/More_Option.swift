//
//  More_Option_ViewController.swift
//  FantaSwype
//
//  Created by appentus on 11/22/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class More_Option: UIView {

    @IBOutlet weak var view_profile_btn:UIButton!
    @IBOutlet weak var share_btn:UIButton!
    @IBOutlet weak var follow_btn:UIButton!
    @IBOutlet weak var block_btn:UIButton!
    @IBOutlet weak var report_btn:UIButton!
    
}



class DefaultPopOverViewController: UIViewController, UIPopoverPresentationControllerDelegate {
    
    init() {
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .popover
        popoverPresentationController?.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UIPopoverPresentationControllerDelegate
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
}
