//  WalkThrough_ViewController.swift
//  FantaSwype
//  Created by appentus on 11/19/19.
//  Copyright © 2019 appentus technologies pvt. ltd. All rights reserved.


import UIKit
import AnimatedCollectionViewLayout

class WalkThrough_ViewController: UIViewController, UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {

//    MARK:- Outlet
    @IBOutlet weak var wt_coll_View:UICollectionView!
    @IBOutlet weak var page_controll:UIPageControl!
    @IBOutlet weak var bottom_constraint_PC:NSLayoutConstraint!
    
    
    
    //    MARK:- vars
    let arr_wt_1_bg = ["wt-1-bg.png","wt-2-bg.png","wt-3-bg.png"]
    let arr_Img_player = ["wt-1.png","wt-2.png","wt-3.png"]
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = AnimatedCollectionViewLayout()
        layout.animator = ParallaxAttributesAnimator()
        layout.scrollDirection = .horizontal
        wt_coll_View.collectionViewLayout = layout
        wt_coll_View.delegate = self
        wt_coll_View.dataSource = self
    }
    
    //  MARK:- action methods
    @IBAction func btn_get_started(_ sender:UIButton) {
        push_To_VC("FantyaSwype", "Login_Types_ViewController")
    }
    
    //  MARK:- UICollectionView methods
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize (width:collectionView.bounds.width, height:collectionView.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! WalkThrough_CollectionViewCell
        
        cell.bottom_constrain.constant = (-(bottom_constraint_PC.constant))+CGFloat(70)
        
        cell.img_wt_1_bg.image = UIImage (named:arr_wt_1_bg[indexPath.row])
        cell.img_player.image = UIImage (named:arr_Img_player[indexPath.row])
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        page_controll.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
    
    
}



